import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // 1
        System.out.print("\nTask 1:\n");
        outProgrammingLanguages();

        // 2
        System.out.print("\nTask 2:\n");
        outDivisions();

        // 3
        System.out.print("\nTask 3:\n");
        outSumAndProduct();

        // 4
        System.out.print("\nTask 4:\n");
        System.out.print("Enter number: ");
        int n = Integer.parseInt(scanner.nextLine());
        outDigitsOfNum(n);

        // 5, 6
        System.out.print("\nTask 6:\n");
        System.out.print("Enter number: ");
        int num = Integer.parseInt(scanner.nextLine());
        System.out.println("Sum of Digits: " + sumOfDigits(num));
        System.out.println("[Recursion] Sum of Digits: " + sumOfDigitsRecursion(num));

        // 7
        System.out.print("\nTask 7:\n");
        outGCDAndLCM();

        // 8
        System.out.print("\nTask 8:\n");
        outSequence();

        // 9
        System.out.print("\nTask 9:\n");
        getMaxAndMin();

        // 10
        System.out.print("\nTask 10:\n");
        outSortedArray();

        // 11
        System.out.print("\nTask 11:\n");
        outRandomIntsArray(8);

        // 12
        System.out.print("\nTask 12:\n");
        outDescendingRandomArray(8);
    }

    // 1
    private static void outProgrammingLanguages() {
        String[] languages = {"C++", "C#", "java", "pascal", "php", "JavaScropt", "ActionScript"};
        for (String language : languages) {
            System.out.print(language + '\n');
        }
    }

    // 2
    private static void outDivisions() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number 1: ");
        int a = Integer.parseInt(scanner.nextLine());
        System.out.print("Enter number 2: ");
        int b = Integer.parseInt(scanner.nextLine());

        int quotient = a / b;
        int remainder = a % b;
        System.out.println(String.format("Quotient: %s", quotient));
        System.out.println(String.format("Remainder: %s", remainder));
    }

    // 3
    private static void outSumAndProduct() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number 1: ");
        int a = Integer.parseInt(scanner.nextLine());
        System.out.print("Enter number 2: ");
        int b = Integer.parseInt(scanner.nextLine());
        System.out.print("Enter number 3: ");
        int c = Integer.parseInt(scanner.nextLine());
        int sum = a + b + c;
        int product = a * b * c;
        System.out.println(String.format("Sum: %s", sum));
        System.out.println(String.format("Product: %s", product));
    }

    // 4
    // This will work for number with any length + recursive
    private static void outDigitsOfNum(int n) {
        if (n > 0) {
            outDigitsOfNum(n / 10);
            System.out.printf("%d\n", (n % 10));
        }
    }

    // 5, 6
    private static int sumOfDigits(int input) {
        int sum = 0;
        while (input > 0) {
            int digit = input % 10;
            sum = sum + digit;
            input = input / 10;
        }
        return sum;
    }

    private static int sumOfDigitsRecursion(int n) {
        if (n == 0) return 0;
        return (n % 10 + sumOfDigits(n / 10));
    }

    // 7
    static int getGCD(int a, int b) {
        if (b == 0) return a;
        else return getGCD(b, a % b);
    }

    private static void outGCDAndLCM() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number 1: ");
        int a = Integer.parseInt(scanner.nextLine());
        System.out.print("Enter number 2: ");
        int b = Integer.parseInt(scanner.nextLine());
        int gcd = getGCD(a, b);
        System.out.println(String.format("GCD: %s", gcd));
        int lcm = Math.abs(a * b) / gcd;
        System.out.println(String.format("LCM: %s", lcm));
    }

    // 8
    static void outSequence() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter M integer: ");
        int m = Integer.parseInt(scanner.nextLine());
        System.out.println("Enter N integer: ");
        int n = Integer.parseInt(scanner.nextLine());
        if (n < m) {
            throw new IllegalArgumentException("N should be larger than or equal to M!");
        }
        for (int j = m + 1; j <= n - 1; j++) { // If we want to include N as well, condition would be: j <= n
            System.out.println(j);
        }
    }

    // 9
    static void getMaxAndMin() {
        int[] nums = {1, 2, 3, 4, 0, 6, 7, 8};

        int max = nums[0];
        for (int i = 1; i < nums.length; i++)
            if (nums[i] > max) max = nums[i];

        int min = nums[0];
        for (int i = 1; i < nums.length; i++)
            if (nums[i] < min) min = nums[i];

        System.out.println(String.format("MAX: %s", max));
        System.out.println(String.format("MIN: %s", min));
    }


    // 10 - without built in sort()
    static void bubbleSort(boolean descending =False) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                int temp;
                if (descending == true) {


                }
                if (nums[i] < nums[j]) {
                    temp = nums[i];
                    nums[i] = nums[j];
                    nums[j] = temp;
                }
            }
        }
    }

    static void outSortedArray() {
        int[] nums = {1, 6, 23, 14, 0, 9, 12, 7};
        // sort ascending with simple bubble sort
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                int temp = 0;
                if (nums[i] > nums[j]) {
                    temp = nums[i];
                    nums[i] = nums[j];
                    nums[j] = temp;
                }
            }
        }
        System.out.print(String.format("Ascending: %s\n", Arrays.toString(nums)));
    }

    // 11
    static void outRandomIntsArray(int count) {
        int[] nums = IntStream.generate(() -> new Random().nextInt(100)).limit(count).toArray();
        System.out.print(String.format("Random integers: %s\n", Arrays.toString(nums)));
    }

    // 12
    static void outDescendingRandomArray(int count) {
        int[] nums = IntStream.generate(() -> new Random().nextInt(100)).limit(count).toArray();
        // sort descending with simple bubble sort

        System.out.print(String.format("Random descending: %s\n", Arrays.toString(nums)));
    }
}
